import React, { useState } from "react";
import "./App.css";
import logo from './logo.svg';


function App() {
  const [VATData, setVATData] = useState({});
  const idRef = React.useRef();
  let VATApiUrl = "https://vat.erply.com/numbers?vatNumber=";

  const getVATWithFetch = async () => {
    const response = await fetch(VATApiUrl);
    const jsonData = await response.json();
    setVATData(jsonData);
  };

  const handleSubmit = async () => {
    VATApiUrl += idRef.current.value;
    getVATWithFetch();
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>Erply VAT number check</h2>
      </header>
      <form onSubmit={e => { e.preventDefault(); }}>
        <label>
          <h1>insert VAT number</h1>
          <input name="name" ref={idRef}/>
        </label>
        <button type="button" onClick={handleSubmit}>Submit</button>

      </form>
      <div className="VAT-container">
        <h5 className="info-item">CountryCode:{VATData.CountryCode}</h5>
        <h5 className="info-item">RequestDate:{VATData.RequestDate}</h5>
        <h5 className="info-item">Valid:{VATData.Valid === true || VATData.Valid === false
          ? '' + VATData.Valid
          : ''}</h5>
        <h5 className="info-item">Name:{VATData.Name}</h5>
        <h5 className="info-item">Address:{VATData.Address}</h5>
      </div>
    </div>
  );
}

export default App;
