## Introduction

The code in this repository contains Test Task given out

## How to Run
To run this project on your machine, open your terminal and follow these steps to get the app running.

- Clone the repo on your machine. 
    ```sh
    git clone https://gitlab.com/Virk0/erply-test-task.git
    ```

- Install dependencies
    ```sh
    yarn install 
    ```
    or
    ```sh
    yarn 
    ```
    or
    ```sh
    npm install 
    ```
- Start the app
    ```sh
    yarn start
    ```
    or
    ```sh
    npm start
    ```
## How to Use
Enter VAT number on the "VAT number" field and then press the "Submit" button
